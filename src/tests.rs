use crate::opts::{Opts, TextMatcher};

use std::io::prelude::*;

#[test]
fn lineno() {
    struct Test {
        re: &'static str,
        want_contains: &'static [&'static str],
        context: (usize, usize),
    }
    const INPUT: &str = "foo\nbar\nbaz\nboo";
    const TESTS: &'static [Test] = &[
        Test {
            re: "bar",
            context: (0, 0),
            want_contains: &["1", "bar"],
        },
        Test {
            re: "haslshaklsdhjsal",
            context: (0, 0),
            want_contains: &[],
        },
        Test {
            re: "bar",
            context: (1, 0),
            want_contains: &["0", "1", "bar", "foo"],
        },
    ];
    for test in TESTS {
        let opts = Opts {
            line_numbers: true,
            matcher: TextMatcher::Regexp(regex::Regex::new(test.re).unwrap()),
            context: test.context,
            verbose: false,
        };
        let mut stdout = Vec::<u8>::new();
        // note that we don't care about stderr, so we use `std::io::sink()` to throw it away.
        // `&[u8]` satisfies io::Read, and `&mut Vec<u8>` satisfies io::Write.
        // You can't use a `&str` or `String` directly, since random bytes might not be valid utf-8.
        crate::run(opts, INPUT.as_bytes(), &mut stdout, &mut std::io::sink()).unwrap();
        let got = String::from_utf8(stdout).unwrap();
        for want in test.want_contains {
            assert!(got.contains(want), "{} should contain {}", got, want)
        }
    }
}
